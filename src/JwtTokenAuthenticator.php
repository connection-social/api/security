<?php

namespace ConnectionSocial\Security;

use ConnectionSocial\Security\Service\SecurityService;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\AuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class JwtTokenAuthenticator extends AbstractAuthenticator implements AuthenticatorInterface
{
    private JWTEncoderInterface $jwtEncoderInterface;

    private ?string $credentials;

    private SecurityService $securityService;

    public function __construct(
        JWTEncoderInterface $jwtEncoderInterface,
        SecurityService $securityService
    ) {
        $this->jwtEncoderInterface = $jwtEncoderInterface;


        $this->securityService = $securityService;
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has('Authorization') && str_contains(
            $request->headers->get('Authorization'),
            'Bearer'
        );
    }

    /**
     * @param Request $request
     *
     * @return void
     * @throws Exception
     */
    private function getCredentials(Request $request): void
    {
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );

        $this->credentials = $extractor->extract($request);

        if ($this->credentials == null) {
            throw new Exception("Invalid token");
        }
    }

    /**
     * @throws Exception
     */
    public function authenticate(Request $request): Passport
    {
        $this->getCredentials($request);

        try {
            $data = $this->jwtEncoderInterface->decode($this->credentials);
        } catch (JWTDecodeFailureException $e) {
            throw new CustomUserMessageAuthenticationException('Invalid Token, We can\'t decode this token');
        }

        if (!$this->securityService->userExist($data['email'], $data['firstName'], $data['lastName'])) {
            throw new CustomUserMessageAuthenticationException('This token not exist', [], 401);
        }

        return new SelfValidatingPassport(new UserBadge($data['email']));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }
}

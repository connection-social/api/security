<?php

namespace ConnectionSocial\Security\Repository;

use ConnectionSocial\Utils\Repository\AbstractRepository;

class SecurityRepository extends AbstractRepository
{
    /**
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     * @return array<string, mixed>
     */
    public function userExist(string $email, string $firstName, string $lastName): array
    {
        $variables = [
            'email'      => $email,
            'first_name' => $firstName,
            'last_name'  => $lastName
        ];
        $options   = [
            self::FETCH_ONE => true
        ];

        return $this->execute(SecuritySQL::SQL_userExist(), $variables, $options);
    }

    /**
     * @param string $email
     * @return array<string, mixed>
     */
    public function findUserByEmail(string $email): array
    {
        $variables = [
            'email' => $email
        ];
        $options   = [
            self::FETCH_ONE => true
        ];

        return $this->execute(SecuritySQL::SQL_findUserByEmail(), $variables, $options);
    }
}

<?php

namespace ConnectionSocial\Security\Repository;
// phpcs:ignoreFile
class SecuritySQL
{
    public static function SQL_userExist(): string
    {
        $table = $_ENV['USER_TABLE'];

        return "
            SELECT
                42
            FROM $table
            WHERE email = :email
                AND first_name = :first_name
                AND last_name = :last_name
        ";
    }
    public static function SQL_findUserByEmail(): string
    {
        $table = $_ENV['USER_TABLE'];

        return "
            SELECT
                id,
                first_name,
                last_name,
                email,
                connection_token,
                password,
                role,
                authorization_get_position
            FROM $table
            WHERE email = :email
        ";
    }
}

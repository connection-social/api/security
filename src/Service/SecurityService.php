<?php

namespace ConnectionSocial\Security\Service;

use ConnectionSocial\Security\Repository\SecurityRepository;
use ConnectionSocial\Utils\Entity\UserEntity;
use ConnectionSocial\Utils\Repository\AbstractRepository;
use ConnectionSocial\Utils\Service\AbstractService;
use Luna\Component\Manager\TypeManager;
use Luna\Component\Manager\ValueManager;

class SecurityService extends AbstractService
{
    /**
     * @var SecurityRepository
     */
    protected AbstractRepository $repository;


    public function __construct(SecurityRepository $securityRepository)
    {
        $this->repository = $securityRepository;
    }

    public function userExist(string $email, string $firstName, string $lastName): bool
    {
        $query = $this->repository->userExist($email, $firstName, $lastName);

        return !TypeManager::isEmpty($query);
    }

    public function userDetailByEmail(string $email): UserEntity
    {
        $query = $this->repository->findUserByEmail($email);

        $user = new UserEntity(
            $query['id'],
            $query['connection_token'],
            $query['role'],
            $query['password'],
            $query['email'],
            $query['first_name'],
            $query['last_name']
        );

        $user->setAuthorizationGetPosition(ValueManager::getBoolean($query['authorization_get_position']));

        return $user;
    }
}
